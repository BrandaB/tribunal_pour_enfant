package com.tribunal.enfant.plateforme_tribunal_enfant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlateformeTribunalEnfantApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlateformeTribunalEnfantApplication.class, args);
	}

}
