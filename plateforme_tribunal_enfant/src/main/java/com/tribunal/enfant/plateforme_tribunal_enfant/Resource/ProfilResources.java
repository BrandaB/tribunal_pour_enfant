package com.tribunal.enfant.plateforme_tribunal_enfant.Resource;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Profil;
import com.tribunal.enfant.plateforme_tribunal_enfant.dao.ProfilsRepository;
@RestController
public class ProfilResources {
	
	@Autowired
	private ProfilsRepository profilsRepository;
	
	@GetMapping("/AllProfils")
	public Page<Profil> getAllProfils(Pageable pageable){
		
		return profilsRepository.findAll(pageable);
		
	}
	@PostMapping("/createProfils")
	
	public Profil CreateProfils(@Valid @RequestBody Profil profil) {
		return profilsRepository.save(profil);
	}
	
	
	@GetMapping("/FindOne")
	public Profil FindOne(Integer id) {
		return profilsRepository.findById(id).get();
	}
	
@PutMapping("/profils/{profilsId}")
public Profil updateProfilById(@PathVariable Integer profilId, @Valid @RequestBody Profil profilRequest) {
	  return profilsRepository.findById(profilId)
	.map(profil  -> {
		profil.setLibelle(profilRequest.getLibelle());
		profil.setDescriptions(profilRequest.getDescriptions());
		return profilsRepository.save(profilRequest);
	}).orElseThrow(() -> new ResourceNotFoundException("profil not found" +profilId));
	
}
@DeleteMapping("/profils/{profilsId}")
public ResponseEntity<?> deleteProfilById(@PathVariable Integer profilId) {
	return profilsRepository.findById(profilId)
			.map(profil -> {
				profilsRepository.delete(profil);
				return ResponseEntity.ok().build();
				
			}).orElseThrow(()-> new ResourceNotFoundException("profil not found"));
	
}

}
