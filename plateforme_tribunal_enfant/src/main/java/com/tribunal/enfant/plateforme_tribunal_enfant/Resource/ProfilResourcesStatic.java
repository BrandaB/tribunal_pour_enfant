package com.tribunal.enfant.plateforme_tribunal_enfant.Resource;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Profil;
import com.tribunal.enfant.plateforme_tribunal_enfant.dao.ProfilDaoServiceStatic;
import com.tribunal.enfant.plateforme_tribunal_enfant.exceptions.Message_Exception;

@Controller
public class ProfilResourcesStatic {
	
	@Autowired
	ProfilDaoServiceStatic profilservice;
/**	
	@GetMapping("/")
	public String index() {
		return "index";
	}**/
	
	@GetMapping("/profils")
	public List<Profil> getAllProfils(){
		return profilservice.findAllList();
	}
	@PostMapping("/Profils")
	public ResponseEntity<Object> saveProfil(@RequestBody Profil p) {	
		Profil profil = profilservice.save(p);
		URI location = ServletUriComponentsBuilder.
				fromCurrentRequest().
				path("{/id}").
				buildAndExpand(profil.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	@GetMapping("/retrieve/{id}")
	public Profil retrieveOneProfil( @PathVariable int id) {
		if(profilservice.findOneProfilById(id)==null)
			throw new Message_Exception("Profil non trouvé");
		return profilservice.findOneProfilById(id);
	}
	
	public void deleteProfil(@PathVariable int id) {
		
		Profil profil =profilservice.deleteById(id);
		if(profil.getId()==null) 
			throw new Message_Exception("Profil non trouvé");
			//throw new Message_All_Exception().
		//statusNotFoundhandleException(new Message_Exception("Profil non trouvé"), WebRequest.);
		}

}
