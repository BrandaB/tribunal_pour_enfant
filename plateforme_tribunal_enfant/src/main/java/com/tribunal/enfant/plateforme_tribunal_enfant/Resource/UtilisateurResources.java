package com.tribunal.enfant.plateforme_tribunal_enfant.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Utilisateur;
import com.tribunal.enfant.plateforme_tribunal_enfant.dao.UtilisateursRepository;
@RestController
public class UtilisateurResources {
	
	@Autowired
	private UtilisateursRepository utilisateursRepository;
	
	@GetMapping("/Allusers")
	public Page<Utilisateur> getAllusers(Pageable pageable){
		return utilisateursRepository.findAll(pageable);
		
	}
	
	@PostMapping("/createUsers")
	public Utilisateur createUsers(@Valid @RequestBody Utilisateur utilisateur ) {
		return utilisateursRepository.save(utilisateur);
		
	}
	
	
	@GetMapping("/findOne")
	public Utilisateur findOne(Integer id) {
		return utilisateursRepository.findById(id).get();
		
	}

}
