package com.tribunal.enfant.plateforme_tribunal_enfant.beans;

import java.util.Date;

public class Message_Exception_Entity {
	
	private Date errordate;
	private String errormessage;
	private String status;
	
	
	
	public Message_Exception_Entity() {
	
	}

	public Message_Exception_Entity(Date errordate, String errormessage, String status) {
		super();
		this.errordate = errordate;
		this.errormessage = errormessage;
		this.status = status;
	}

	public Date getErrordate() {
		return errordate;
	}

	public String getErrormessage() {
		return errormessage;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "Message_Exception_Entity [errordate=" + errordate + ", errormessage=" + errormessage + ", status="
				+ status + "]";
	}

}
