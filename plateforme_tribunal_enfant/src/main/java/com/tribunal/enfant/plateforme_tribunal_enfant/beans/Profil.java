package com.tribunal.enfant.plateforme_tribunal_enfant.beans;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
@Entity
@Table(name="profils")
public class Profil extends AuditModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	@Size(min=2, message="name should have at least 2 characters")
	private String libelle;
	
	@Size(min=2, message="name should have at least 2 characters")
	private String descriptions;
	
	
	
	
	public Profil()  {
		super();
	
	}

	public Profil(Integer id, String libelle, String descriptions, Date lastdate) {
		this.id = id;
		this.libelle = libelle;
		this.descriptions = descriptions;
	}

	public Profil(Integer id, String libelle, String descriptions) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.descriptions = descriptions;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	@Override
	public String toString() {
		return "Profil [id=" + id + ", libelle=" + libelle + ", descriptions=" + descriptions + "]";
	}

	
	
	
	

}
