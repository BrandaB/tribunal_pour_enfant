package com.tribunal.enfant.plateforme_tribunal_enfant.dao;

import java.util.*;

import org.springframework.stereotype.Component;

import com.tribunal.enfant.plateforme_tribunal_enfant.beans.Profil;

@Component
public class ProfilDaoServiceStatic {
	
	private static List<Profil> profils = new ArrayList<>();
	private static int profilid=4;
	
	static{
		profils.add(new Profil(1,"Administrateur","Administrateur du Systeme"));
		profils.add(new Profil(2,"Greffier en chef","Greffier"));
		profils.add(new Profil(3,"Juge d'instruction","Juge"));
		profils.add(new Profil(4,"Procureur de la republique","Procureur"));
	}
	
	public List<Profil> findAllList(){
		return profils;
	}
	
	public Profil save(Profil p) {
		if(p.getId()==null)
			p.setId(profilid++);
		profils.add(p);
		return p;
	}
	
	public Profil findOneProfilById(int id) {
		for(Profil profil: profils) 
			if(profil.getId()==id)
				return profil;
		return null;
	}
	
	public Profil deleteById(int id) {
		Iterator<Profil> iterator= profils.iterator();
		while(iterator.hasNext()) {
			Profil profil = iterator.next();
			if(profil.getId()==id) {
				iterator.remove();
				return profil;
			}
		}
		return null;
	}

}
